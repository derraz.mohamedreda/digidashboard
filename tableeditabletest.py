# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 15:25:37 2020

Test de graphique avec mathplotlib pour le tableau editable

@author: Nicolas, Reda
"""

import pandas as pd
import matplotlib as plt

tableau = {
    'A': [1, 2, 3, 4], 
    'B': [5, 6, 7, 8], 
    'C': [9, 10, 11, 12],
    'D': [13, 14, 15, 16]
    }
df = pd.DataFrame(data=tableau)

print(df)

df.plot()
