(async()=>{

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
const testfetch = await axios.get('/datahistogrammes');

const dataTest = testfetch.data;

// Boucle qui itère sur les données du Json
for(let i = 0; i < dataTest.length; i++){

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
  let newDiv = document.createElement('div');
  newDiv.id = dataTest[i].table;
  document.body.appendChild(newDiv);

  AmCharts.makeChart(dataTest[i].table, {
    "type": "serial",
    "theme": "light",
    "columnWidth": 1,
    "dataProvider": dataTest[i].data,
    "graphs": [{
      "fillColors": "#c55",
      "fillAlphas": 0.9,
      "lineColor": "#fff",
      "lineAlpha": 0.7,
      "type": "column",
      "valueField": "count"
    }],
    "categoryField": "category",
    "categoryAxis": {
      "startOnAxis": true,
      "title": "Try"
    },
    "valueAxes": [{
      "title": "Count"
    }]
  })

}



/*
var chart = AmCharts.makeChart("histogramme1", {
  "type": "serial",
  "theme": "light",
  "columnWidth": 1,
  "dataProvider": blabla,
  "graphs": [{
    "fillColors": "#c55",
    "fillAlphas": 0.9,
    "lineColor": "#fff",
    "lineAlpha": 0.7,
    "type": "column",
    "valueField": "count"
  }],
  "categoryField": "category",
  "categoryAxis": {
    "startOnAxis": true,
    "title": "Try"
  },
  "valueAxes": [{
    "title": "Count"
  }]
});

/*
var chart = AmCharts.makeChart("histogramme2", {
  "type": "serial",
  "theme": "light",
  "columnWidth": 1,
  "dataProvider": [{
    "category": "0"
  }, {
    "category": "1",
    "count": 17
  }, {
    "category": "2",
    "count": 10
  }, {
    "category": "3",
    "count": 41
  }, {
    "category": "4",
    "count": 32
  }, {
    "category": "5",
    "count": 50
  }, {
    "category": "6",
    "count": 40
  }, {
    "category": "7",
    "count": 78
  }, {
    "category": "8",
    "count": 36
  }, {
    "category": "9",
    "count": 65
  }, {
    "category": "10",
    "count": 14
  }, {
    "category": "11"
  }],
  "graphs": [{
    "fillColors": "#4B06B4",
    "fillAlphas": 0.9,
    "lineColor": "#fff",
    "lineAlpha": 0.7,
    "type": "column",
    "valueField": "count"
  }],
  "categoryField": "category",
  "categoryAxis": {
    "startOnAxis": true,
    "title": "Try"
  },
  "valueAxes": [{
    "title": "Count"
  }]
});
*/
})()
