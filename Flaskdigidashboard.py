# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

Module Flask permettant à un utilisateur de créer un dashboard simplement
à l'aide du wrapper (DigiDashBoard.py).

L'utilisateur n'aura pas besoin d'utiliser Flask pour créer son dashboard,
seulement d'appeler les classes du wrapper (DigiDashBoard.py).

@author: Nicolas, Reda
"""

from flask import Flask, render_template, json

app = Flask(__name__)

boutonstitres = {}   
graphiques = {}


@app.route('/')
def index():
    
    return render_template('accueil.html', title='Accueil',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

@app.route('/accueil')
def accueil():
  
    return render_template('accueil.html', title='Accueil', 
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

@app.route('/Histogramme')
def histogrammes():
    
    return render_template('Histogramme.html', title='Histogrammes',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

# Cette fonction permet la récupération du JSON présent dans la racine du 
# dossier, puis avec l'app.route de le retouner pour JS grace au "GET".
# "/datahistogrammes" est la route utilisée dans le fichier JS.
@app.route('/datahistogrammes', methods=['GET'])
def datahistogrammes():
    
    hist1 = open('json/histogrammes/histogramme1.json',)
    histogramme1 = json.load(hist1)
    histodump = json.dumps(histogramme1)
    
    return (histodump)

@app.route('/Scatter')
def scatter():
  
    return render_template('Scatter.html',title='Scatter',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

@app.route('/datascatter', methods=['GET'])
def datascatter():
    
    scatter = open('json/scatter/scatter1.json',)
    scatter1 = json.load(scatter)
    scatdump = json.dumps(scatter1)
    
    return (scatdump)

@app.route('/Courbe')
def courbe():
  
    return render_template('Courbe.html', title='Courbe',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

@app.route('/datacourbe', methods=['GET'])
def datascourbe():
    
    cour1 = open('json/courbes/courbe1.json',)
    courbe = json.load(cour1)
    courbedump = json.dumps(courbe)
    
    return (courbedump)


@app.route('/Diagrammes_circulaires')
def diagrammes_circulaires():
  
    return render_template('Diagrammes_circulaires.html', 
                           title='Diagrammes circulaires',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

@app.route('/datadiagrammes_circulaires', methods=['GET'])
def datadiagrammes_circulaires():
    
    dcirc = open('json/dcirculaires/dcirculaire1.json',)
    dcirculaire = json.load(dcirc)
    dcirculairedump = json.dumps(dcirculaire)
    
    return (dcirculairedump)

@app.route('/Table')
def table():
  
    return render_template('Table.html', title='Table',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)

@app.route('/datatable', methods=['GET'])
def datatable():
    
    table = open('json/tables/table.json',)
    tablej = json.load(table)
    tablejsonn = json.dumps(tablej)
    
    return (tablejsonn)

@app.route('/Table_dynamique')
def table_dynamique():
    
    """Test jinja data tableau"""
    
    # datatest = [
    #             [ 1994, 1587, 650, 121 ],
    #             [ 1995, 1567, 683, 146 ],
    #             [ 1996, 1617, 691, 138 ],
    #             [ 1997, 1630, 642, 127 ],
    #             [ 1998, 1660, 699, 105 ],
    #             [ 1999, 1683, 721, 109 ],
    #             [ 2000, 1691, 737, 112 ],
    #             [ 2001, 1298, 680, 101 ],
    #             [ 2002, 1275, 664, 97 ],
    #             [ 2003, 1246, 648, 93 ],
    #             [ 2004, 1318, 697, 111 ],
    #             [ 2005, 1213, 633, 87 ],
    #             [ 2006, 1199, 621, 79 ],
    #             [ 2007, 1110, 210, 81 ],
    #             [ 2008, 1165, 232, 75 ],
    #             [ 2009, 1145, 219, 88 ],
    #             [ 2010, 1163, 201, 82 ],
    #             [ 2011, 1180, 285, 87 ],
    #             [ 2012, 1159, 277, 71 ]
    #            ]
    
    # datajsonn = json.load(datatest)
    # datadump = json.dumps(datatest)
    
    
    
    return render_template('Table_dynamique.html', title='Table_dynamique',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques)
