#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 15:59:56 2020

@author: dev

Pour faire une première proposition, on créé une structure de données fictives
comprenant un ensemble d'achats :
    date d'achat
    nom de client
    montant de la vente
    réduction si réduction appliquée
    prix fournisseur du produit
    marge (différence entre le prix fournisseur et la vente)
    id de produit

ainsi qu'un stock de produits (ici des flotteurs de planche à voile) 
comprenant :
    année
    une marque
    un type de planche (construction)
    un volume
    un prix d'achat
    un prix de vente
    un id_produit
"""
import datetime as date
import numpy as np
import pandas as pd
from datetime import datetime, timedelta

### Génération données de produits
def generation_produits():
    """ Les marques, types de flotteur, volumes et années ne sont pas générés. 
    Les prix sont générés aléatoirement entre un minimum et un maximum 
    ~réaliste, une différence est faite entre le prix d'achat au fournisseur 
    et le prix de vente en magasin. 
    La configuration actuelle génère 24 produits (flotteurs de planche à 
    voile) stockés dans une dataframe pandas.
    
    TODO peut-être :
        - rajouter un type de planche (wave/race/etc.)
        - rajouter un nom pour chaque modèle ?
    """
    marques = ["JP Australia"]
    types_flotteur = ["bois", "carbone"]
    volumes = [110, 120, 130, 140]
    # modèles des 3 dernières années
    annees = [2018, 2019, 2020]
    marge = 0.40
    np.random.seed(0)
    produits = {}
    caracs = []
    id_produit = 0
    for marque in marques:  
        for type_flotteur in types_flotteur:
            for volume in volumes:
                for annee in annees:
                    caracs = []
                    # prix de vente est égal au prix d'achat + 40% de marge
                    prix_achat = int(np.random.randint(1700, 2499, 1))
                    prix_vente = prix_achat + (prix_achat*marge)
                    # tout ce qui descend plus compacte
                    caracs.append(annee)
                    caracs.append(marque)
                    caracs.append(type_flotteur)
                    caracs.append(volume)
                    caracs.append(prix_achat)
                    caracs.append(prix_vente)
                    caracs.append(id_produit)
                    # ajoute au dico produits un id en clé et une liste
                    # de caractéristiques en valeur
                    produits[id_produit] = caracs
                    id_produit += 1
                    
    colonnes = ["année", "marque", "construction", "volume", 
                "prix_achat", "prix_vente", "id_produit"]
    
    return pd.DataFrame.from_dict(produits, orient='index', columns=colonnes)


### Génération données de ventes
def generation_ventes(donnees_produits, date_debut, date_fin, frequence):
    """
    A partir d'une liste de noms fictifs et de dates, on génère des achats 
    faisant réfèrence aux produits générés précedemment.
    """
    noms = ["Marteau", "Riou", "Guitton", "Fleury", "Hamin", 
            "Rollet", "Bouteille", "Lair", "Arlan", "Champion", 
            "Julien", "Dia", "Martel", "Pepin", "Iris"]    
    # freq -> fréquence d pour day, M pour month (MS)
    dates = pd.date_range(date_debut, date_fin - timedelta(days=1), 
                          freq=frequence)
    # coefs correspond aux réductions qui peuvent être faites lors de la vente
    coefs = [-0.20, -0.15, 0, -0.10, -0.05]
    
    id_achat = 0
    achats = {}
    for date_achat in dates:      
        # récup d'un produit aléatoirement dans la df produits
        produit = donnees_produits.sample(1)
        # choisit une réduction aléa en prenant en compte les probas
        coef = np.random.choice(coefs, p=[0.05, 0.05, 0.70, 0.1, 0.1])
        # calcul du prix de vente en fonction de la réduc (si réduc)
        prix_vente = produit["prix_vente"] + produit["prix_vente"] * coef
        # calcul de la marge 
        benef = prix_vente - produit["prix_achat"]
        achats[id_achat] = [np.random.choice(noms), 
                    date_achat,
                    int(prix_vente),
                    int(coef*100),
                    int(produit["prix_achat"]),
                    int(benef),
                    int(produit["id_produit"]),
                    date_achat.year,
                    date_achat.month]
        id_achat += 1
        
    colonnes = ["client", "date_achat", "prix_vente", "réduction", 
                "prix_achat", "marge", "id_produit", "annee", "mois"]
    
    return pd.DataFrame.from_dict(achats, orient='index', columns=colonnes)


### Génération données pour corrélation
def generation_data_corr(date_debut, date_fin):
    """
    Génère des achats de combinaison et/ou crème solaire sur 365 jours de 
    l'année, avec une probas d'acheter les deux de 80% si la date d'achat se 
    trouve entre le 15 juin et le 15 sept.
    """
    noms = ["Marteau", "Riou", "Guitton", "Fleury", "Hamin", 
            "Rollet", "Bouteille", "Lair", "Arlan", "Champion", 
            "Julien", "Dia", "Martel", "Pepin", "Iris"]
    # freq -> fréquence d pour day, M pour month (MS)
    dates = pd.date_range(date_debut, date_fin - timedelta(days=1), freq='d')
    achats = {}
    probas = {"été": [0.2, 0.8], "hiver": [1, 0]}
    id_achat = 0
    for date_achat in dates:      
        if date.datetime(2020, 6, 1) <=date_achat<= date.datetime(2020, 9, 1):
            saison = probas["été"]
            articles = ["crème solaire", "combinaison été"]
        else:
            saison = probas["hiver"]
            articles = ["combinaison hiver"]
        quantite = np.random.choice([1, 2], p=saison)
        produits = np.random.choice(articles, quantite, replace=False)
        client = np.random.choice(noms)
        combi_été, combi_hiver, protec_sol = 0, 0, 0
        if len(produits) == 2:
            combi_été, protec_sol = 1, 1
        elif produits == "combinaison été":
            combi_été = 1
        elif produits == "combinaison hiver":
            combi_hiver = 1
        elif produits == "crème solaire":
            protec_sol = 1
        achats[id_achat] = [date_achat,
                                  client, 
                                  combi_été,
                                  combi_hiver,
                                  protec_sol,
                                  date_achat.year,
                                  date_achat.month]
        id_achat += 1
    
    colonnes = ["date_achat", "client", "combi été", "combi hiver", 
                "protec sol", "annee", "mois"]

    return pd.DataFrame.from_dict(achats, orient='index', columns=colonnes)


### Génération données pour optimisation stock/commande
def generation_ventes_opti(date_debut, date_fin):
   """
   Génère un journal des ventes exploitable pour l'optimisation et les graphs
   du second module.
   """
   data = {
        'Produit':
                   ['DYNAMX EPS', 'CHASE PE SNIPER', 'BLACKWIDOW NRG SNIPER', 
                   'RAY AIR PREMIUM FANATIC', 'MATIRA LW 10', 
                   '6/7 SHORTBOARD BIC SPORT', '6 QUAD FISH EARTH BIC SPORT',
                   '7/3 MINI MALIBU BIC SPORT','S-QUAD 2018','MITU 2018',
                   'MAJESTIC FULLSUIT 5/3 FZ 2018', 'PYRO', 'IDOL LTD 2018',
                   'RADAR 2018'],
            'Prix':
                   [59.90, 115, 249, 
                   1399, 575.40,
                   309, 749, 339, 1029,
                   999, 262.49, 224.95,
                   789, 1049],
            'Catégorie': 
                   ['BODYBOARD', 'BODYBOARD', 'BODYBOARD', 'SUP',
                   'SUP', 'SURF', 'SURF', 'SURF', 'SURF', 'SURF',
                   'COMBI_SURF', 'HARNAIS', 'VOILE', 'AILE']      
               }
   df = pd.DataFrame(data)
   noms = ["Marteau", "Riou", "Guitton", "Fleury", "Hamin", 
                "Rollet", "Bouteille", "Lair", "Arlan", "Champion", 
                "Julien", "Dia", "Martel", "Pepin", "Iris"]    
    # freq -> fréquence d pour day, M pour month (MS)
   dates = pd.date_range(date_debut, date_fin - timedelta(days=1), 
                          freq="d")
   coefs = [-0.20, -0.15, 0, -0.10, -0.05]
   id_achat = 0
   achats = {}
   for date_achat in dates: 
       quantite_achat = np.random.randint(1, 5, 1)
       produits = df.sample(quantite_achat)
       coef = np.random.choice(coefs, p=[0.05, 0.05, 0.70, 0.1, 0.1])
       for produit, prix, categorie in zip(produits['Produit'], 
                                           produits['Prix'], 
                                           produits['Catégorie']):
           prix_vente = prix + prix * coef
           prix_achat = prix - (prix * 0.40)
           marge = prix_vente - prix_achat
           achats[id_achat] = [produit,
                               np.random.choice(noms), 
                               date_achat,
                               int(prix_vente),
                               int(prix_achat),
                               int(marge),
                               int(coef*100),
                               categorie,
                               date_achat.year,
                               date_achat.month]
           id_achat += 1
        
   colonnes = ["produit", "client", "date_achat", "prix_vente", "prix_achat", 
               "marge", "réduction", "catégorie", "annee", "mois"]

   return pd.DataFrame.from_dict(achats, orient='index', 
                                             columns=colonnes)


### TEST
if __name__ == "__main__" :
    test_analyse_mensuelle = False
    generation_donnees = False
    generation_correlation = True
    generation_opti_ventes = False
               
    if generation_donnees:
        produits_fictifs = generation_produits()
        # d pour day, m pour month, ms pour month start
        frequence = "d"
        date_debut = date.datetime(2018, 1, 1)
        date_fin = date.datetime(2020, 12, 31)
        achats_fictifs = generation_ventes(produits_fictifs, date_debut, 
                                           date_fin, frequence)
    if generation_correlation:
        date_debut = date.datetime(2020, 1, 1)
        date_fin = date.datetime(2020, 12, 31)
        donnees_correlees = generation_data_corr(date_debut, date_fin)
        
    if generation_opti_ventes:
        date_debut = date.datetime(2020, 1, 1)
        date_fin = date.datetime(2020, 12, 31)
        journal_ventes = generation_ventes_opti(date_debut, date_fin)
         
        
        