# -*- coding: utf-8 -*-
'''
Created on Sun Jul 12 21:25:48 2020

DATA DE L'HISTOGRAMME1 DU JS MODIFIEE

Test de la création d'un Json utilisé pour un graphique d'un JS, depuis une
dataframe, array...

@author: Reda
'''

import pandas as pd
import json
import numpy as np



hist1 = [{
    "category": "0"
  }, {
    "category": "1",
    "count": 23
  }, {
    "category": "2",
    "count": 12
  }, {
    "category": "3",
    "count": 5
  }, {
    "category": "4",
    "count": 70
  }, {
    "category": "5",
    "count": 40
  }, {
    "category": "6",
    "count": 2
  }, {
    "category": "7",
    "count": 5
  }, {
    "category": "8",
    "count": 85
  }, {
    "category": "9",
    "count": 50
  }, {
    "category": "10",
    "count": 60
  }, {
    "category": "11"
  }]
      
hist2 = [{
    "category": "0"
  }, {
    "category": "1",
    "count": 17
  }, {
    "category": "2",
    "count": 10
  }, {
    "category": "3",
    "count": 41
  }, {
    "category": "4",
    "count": 32
  }, {
    "category": "5",
    "count": 50
  }, {
    "category": "6",
    "count": 40
  }, {
    "category": "7",
    "count": 78
  }, {
    "category": "8",
    "count": 36
  }, {
    "category": "9",
    "count": 65
  }, {
    "category": "10",
    "count": 14
  }, {
    "category": "11"
  }]


scatter1 = [
          ['Age', 'Weight'],
          [ 4,      10],
          [ 8,      15],
          [ 12,     25],
          [ 15,      55],
          [ 18,      60],
          [ 20,    64],
          [ 25,    70],
          [ 30,    75],
          [ 35,    80],
          [ 50,    85],
        ]  

courbe1 = [{
  "x": 5,
  "y": 13
}, {
  "x": 10,
  "y": 11
}, {
  "x": 20,
  "y": 15
}, {
  "x": 30,
  "y": 16
}, {
  "x": 40,
  "y": 18
}, {
  "x": 50,
  "y": 13
}, {
  "x": 60,
  "y": 22
}, {
  "x": 70,
  "y": 25
}]
    
df = pd.DataFrame(data=hist2)

print(df)

courbe1 = json.dumps(courbe1)

test = df.to_json(orient='records')

with open('courbe1.json', 'w') as outfile:
    json.dump(courbe1, outfile)

# Generation alétoire d'array
randscat = np.random.randint(40, size=(15, 2))
test =[[38, 32],
       [24, 39],
       [36, 34],
       [29, 25],
       [27, 27],
       [31,  5],
       [38, 29],
       [ 1,  4],
       [35, 32],
       [19,  5],
       [16, 25],
       [15, 22],
       [35, 25],
       [25,  9],
       [29, 29]]
np.sort([randscat])

test1 = sorted(test, key=lambda x: x[0])


diagrammes = [
  {
    "country": "France",
    "litres": 150
  },
  {
    "country": "Angleterre",
    "litres": 170
  },
  {
    "country": "Espagne",
    "litres": 200
  },
  {
    "country": "Italie",
    "litres": 160
  },
  {
    "country": "Allemagne",
    "litres": 120
  },
  {
    "country": "Portugal",
    "litres": 115
  }
]

diagrammes = json.dumps(diagrammes)

test = df.to_json(orient='records')

jsonn = open('histogramme1.json',)
jsonn1 = json.load(jsonn)
jsonn2 = json.dumps(jsonn1)

with open('histogramme1.json') as json_file:
    dumper = json.load(jsonn)

with open('json/histogrammes/histogrammeTEST.json', 'w') as outfile:
    json.dump(jsonn1, outfile)
    
