# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

La page utilisateur, il n'a qu'appeler les classes pour créer son dashboard.

Ne pas oublié d'importer DigiDashBoard.

TODO : Utiliser Vu.js pour avoir un mecanisme de tableau modifiable (input),
si des graphes sont affiché, le résultat changeen conséquence.

(modifier les données d'un graph avec un tableau (mathplotlib))

@author: Nicolas, Reda
"""

import DigiDashBoard as DDB
import Flaskdigidashboard as fdb

graphiques = {
             "graphiques1" : "Histogramme",
             "graphiques2" : "Scatter",
             "graphiques3" : "Courbe",
             "graphiques4" : "Diagrammes_circulaires",
             "graphiques5" : "Table"
             }


boutonstitres = {
                "namedb1" : "Digi",
                "namedb2" : "DashBoard",
                "machine" : "Mon Pc portable",
                "bouton1" : "Dashboards",
                "bouton6" : "Stock",
                "bouton7" : "Chiffres",
                "soustitre1" : "Dashboards",
                "soustitre2" : "Profile",
                "soustitre3" : "Graphiques",
                "soustitre4" : "Gestion stock",
                "soustitre5" : "Commerce",
                "titrehisto" : "Bienvenue sur votre page d'histogrammes",
                "textehisto" : "Cette page regroupe les histogrammes du Digidashboard, c'est un test. Vous avez ici 6 histogrammes pour le moment.",
                "titrescatter" : "Bienvenue sur votre page de scatter",
                "textescatter" : "Cette page regroupe les scatter du Digidashboard, c'est un test. Vous avez ici 6 scatter pour le moment.",
                "titrecourbe" : "Bienvenue sur votre page de courbes",
                "textecourbe" : "Cette page regroupe les courbes du Digidashboard, c'est un test. Vous avez ici 6 courbes pour le moment.",
                "titrediagrammecirc" : "Bienvenue sur votre page des diagrammes circulaires",
                "textediagrammecirc" : "Cette page regroupe les diagrammes circulaires du Digidashboard, c'est un test. Vous avez ici 6 diagrammes circulaires pour le moment."
                }


boutonstitres = DDB.Boutonstitres(boutonstitres)
graphiques = DDB.Graphiques(graphiques)



boutonstitres.initialisation()
graphiques.initialisation()


if __name__ == '__main__':
    fdb.app.run (debug = False, use_reloader=False)